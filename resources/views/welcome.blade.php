<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="description" content="Ghana News, Visit Peace FM News, breaking news and feature stories. Also entertainment, business, technology and health news, Ndc Presidential Primaries 2019" />
        <meta name="keywords" content="Ghana News, Peace FM online news, breaking news,  Ghana news,  peacefm news, npp, ndc, cpp, pnc, ghana politics,  UTV,Ndc Presidential Primaries 2019" />
        <title>Akosombo Power</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body style="background-color: white">
        <div class="flex-center position-ref full-height">

            <div class="content">
                <div class="title m-b-md">
                    Akosombo Power - GH
                </div>
                <img src="images/undercons.jpeg">
                <p style="color: red;font-family: 'arial';font-size: large">Online Sales of Electrical Hardware, Lights and Services</p>

            </div>
        </div>
    </body>
</html>
